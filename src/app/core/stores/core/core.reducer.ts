import { CoreState } from './core.model';
import { CoreActions, CoreActionTypes } from './core.actions';

export const initialState: CoreState = {
  champions: [],
  selectedChampion: null
};

export function coreReducer(state: CoreState = initialState, action: CoreActions): CoreState {
  switch (action.type) {
    case CoreActionTypes.LOAD_ALL_CHAMPIONS:
      return { ...state, champions: action.payload };

    case CoreActionTypes.LOAD_CHAMPION_BY_ID:
      return { ...state, selectedChampion: action.payload };

    default:
      return state;
  }
}
