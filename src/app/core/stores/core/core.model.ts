import { Champion } from 'src/app/models/champion';

export interface CoreState {
  champions: Array<Champion>;
  selectedChampion: Champion;
}
