import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import {
  GetAllChampionsAction,
  CoreActionTypes,
  GetChampionByIdAction,
  LoadAllChampionsAction,
  LoadChampionByIdAction
} from './core.actions';
import { defer, of } from 'rxjs';
import { ApiService } from '../../services/api/api.service';
import { Champion } from 'src/app/models/champion';
import { AppState } from 'src/app/core.state';

@Injectable()
export class CoreEffects {
  constructor(
    private actions$: Actions<Action>,
    private router: Router,
    private apiService: ApiService,
    private store: Store<AppState>
  ) {}

  @Effect({ dispatch: false })
  getAllChampions = this.actions$.pipe(
    ofType<GetAllChampionsAction>(CoreActionTypes.GET_ALL_CHAMPIONS),
    tap(() => {
      this.apiService.getAllChampions().subscribe((champions: Array<Champion>) => {
        this.store.dispatch(new LoadAllChampionsAction(champions));
      });
    })
  );

  @Effect({ dispatch: false })
  getChampionById = this.actions$.pipe(
    ofType<GetChampionByIdAction>(CoreActionTypes.GET_CHAMPION_BY_ID),
    tap(res => {
      const champId = res.id;
      this.apiService.getChampionById(champId).subscribe((selectedChampion: Champion) => {
        this.store.dispatch(new LoadChampionByIdAction(selectedChampion));
        this.router.navigate(['/champions', champId]);
      });
    })
  );
}
