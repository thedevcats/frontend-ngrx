import { Action } from '@ngrx/store';
import { Champion } from 'src/app/models/champion';

export enum CoreActionTypes {
  GET_ALL_CHAMPIONS = '[Core] Get all champions',
  LOAD_ALL_CHAMPIONS = '[Core] Load champions',
  GET_CHAMPION_BY_ID = '[Core] Get champion by ID',
  LOAD_CHAMPION_BY_ID = '[Core] Load champion by ID'
}

export class GetAllChampionsAction implements Action {
  readonly type = CoreActionTypes.GET_ALL_CHAMPIONS;
}

export class LoadAllChampionsAction implements Action {
  readonly type = CoreActionTypes.LOAD_ALL_CHAMPIONS;
  constructor(public payload: Array<Champion>) {}
}

export class GetChampionByIdAction implements Action {
  readonly type = CoreActionTypes.GET_CHAMPION_BY_ID;
  constructor(public id: string) {}
}

export class LoadChampionByIdAction implements Action {
  readonly type = CoreActionTypes.LOAD_CHAMPION_BY_ID;
  constructor(public payload: Champion) {}
}

export type CoreActions =
  | GetAllChampionsAction
  | GetChampionByIdAction
  | LoadAllChampionsAction
  | LoadChampionByIdAction;
