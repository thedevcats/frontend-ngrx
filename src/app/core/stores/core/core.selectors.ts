import { createSelector } from '@ngrx/store';
import { CoreState } from './core.model';
import { selectCoreState } from 'src/app/core.state';

export const champions = createSelector(selectCoreState, (state: CoreState) => state.champions);

export const selectedChampion = createSelector(
  selectCoreState,
  (state: CoreState) => state.selectedChampion
);
