import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Champion } from 'src/app/models/champion';
import { Observable } from 'rxjs';
import { GetChampionByIdAction } from '../../stores/core/core.actions';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/core.state';
import { selectedChampion } from '../../stores/core/core.selectors';

@Injectable({
  providedIn: 'root'
})
export class DetailsResolver implements Resolve<any> {
  selectedChampion$: Observable<Champion>;
  selectedChampion: Champion;

  constructor(private store: Store<AppState>) {
    this.store.pipe(select(selectedChampion)).subscribe(d => (this.selectedChampion = d));
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;

    if (!this.selectedChampion || this.selectedChampion.id !== id) {
      this.store.dispatch(new GetChampionByIdAction(id));
    }
  }
}
