import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getAllChampions() {
    return this.http.get('/champions');
  }

  getChampionById(id: string) {
    return this.http.get(`/details/${id}`);
  }
}
