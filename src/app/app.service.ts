import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Champion } from './models/champion';
import { AppState } from './core.state';
import { Store, select } from '@ngrx/store';
import { champions, selectedChampion } from './core/stores/core/core.selectors';
import { GetAllChampionsAction, GetChampionByIdAction } from './core/stores/core/core.actions';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  champions$: Observable<Array<Champion>> = this.store.pipe(select(champions));
  selectedChampion$: Observable<Champion> = this.store.pipe(select(selectedChampion));

  constructor(private store: Store<AppState>) {
    this.store.dispatch(new GetAllChampionsAction());
  }

  navigateToDetails(id: string) {
    this.store.dispatch(new GetChampionByIdAction(id));
  }
}
