import { Component } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-champion-dashboard',
  templateUrl: './champion-dashboard.component.html',
  styleUrls: ['./champion-dashboard.component.scss']
})
export class ChampionDashboardComponent {
  champions$ = this.appService.champions$;

  constructor(private appService: AppService) {}

  navigateToDetails(id: string) {
    this.appService.navigateToDetails(id);
  }
}
