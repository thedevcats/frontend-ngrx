import { ActionReducerMap, MetaReducer, createFeatureSelector } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';

import { RouterStateUrl } from './core/stores/router/router.state';
import { CoreState } from './core/stores/core/core.model';
import { coreReducer } from './core/stores/core/core.reducer';
import { environment } from 'src/environments/environment';
import { debug } from './debug.reducer';

export const reducers: ActionReducerMap<AppState> = {
  core: coreReducer,
  router: routerReducer
};

export const metaReducers: MetaReducer<AppState>[] = [];
if (!environment.production) {
  metaReducers.unshift(storeFreeze);
  if (environment.production) {
    metaReducers.unshift(debug);
  }
}

export const selectCoreState = createFeatureSelector<AppState, CoreState>('core');

export const selectRouterState = createFeatureSelector<
  AppState,
  RouterReducerState<RouterStateUrl>
>('router');

export interface AppState {
  core: CoreState;
  router: RouterReducerState<RouterStateUrl>;
}
