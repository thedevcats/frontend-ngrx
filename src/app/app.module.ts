import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FakeBackendProvider } from './core/interceptors/fake-backend/fake-backend.interceptor';
import { ChampionDashboardComponent } from './components/champion-dashboard/champion-dashboard.component';
import { ChampionDetailsComponent } from './components/champion-details/champion-details.component';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, metaReducers } from './core.state';
import { CoreEffects } from './core/stores/core/core.effects';

@NgModule({
  declarations: [AppComponent, ChampionDashboardComponent, ChampionDetailsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({
      name: 'League of Legends'
    }),
    EffectsModule.forRoot([CoreEffects])
  ],
  providers: [FakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule {}
